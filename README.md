# 组件库

***工具包、DB封装、MQ封装、IO框架、RPC封装***

### 基本环境

- springboot      2.5.4
- springCloud     Hoxton.SR9

### 项目库结构

使用tree命令显示项目结构

```
tree -d -L 2
```

```
├── common
├── db
│   ├── db-hbase
│   ├── db-jpa
│   ├── db-jpa-mysql
│   ├── db-jpa-oracle
│   ├── db-mongo
│   └── db-mybatis
├── gradles
├── io
│   ├── io-api
│   ├── io-base
│   ├── io-core
│   ├── io-mq
│   └── io-store
├── mq
│   ├── mq-base
│   ├── mq-kafka
│   ├── mq-local
│   ├── mq-ons
│   ├── mq-rabbitmq
│   ├── mq-redis
│   ├── mq-rocketmq
│   └── mq-starter
├── rpc
│   ├── rpc-grpc
│   └── rpc-thrift
└── utils
    ├── utils-base
    ├── utils-disruptor
    ├── utils-dubbo
    ├── utils-excel
    ├── utils-fastdfs
    ├── utils-feign
    ├── utils-gps
    ├── utils-httpclient
    ├── utils-redis
    ├── utils-rxtx
    ├── utils-spring
    └── utils-task


```