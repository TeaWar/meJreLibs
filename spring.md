### Spring Cloud与Spring Boot版本匹配关系

官网版本对应地址：https://start.spring.io/actuator/info

springCloud版本 | springBoot版本要求
---|---
2022.0.0-M2 | Spring Boot >=3.0.0-M2 and <3.1.0-M1
2022.0.0-M1	| Spring Boot >=3.0.0-M1 and <3.0.0-M2
2021.0.3	| Spring Boot >=2.6.1 and <3.0.0-M1
2021.0.0-RC1	| Spring Boot >=2.6.0-RC1 and <2.6.1
2021.0.0-M3	| Spring Boot >=2.6.0-M3 and <2.6.0-RC1
2021.0.0-M1	| vSpring Boot >=2.6.0-M1 and <2.6.0-M3
2020.0.5	| Spring Boot >=2.4.0.M1 and <2.6.0-M1
Hoxton.SR12	| Spring Boot >=2.2.0.RELEASE and <2.4.0.M1
Hoxton.BUILD-SNAPSHOT	| Spring Boot >=2.2.0.BUILD-SNAPSHOT
Hoxton.M2	| Spring Boot >=2.2.0.M4 and <=2.2.0.M5
Greenwich.BUILD-SNAPSHO	| Spring Boot >=2.1.9.BUILD-SNAPSHOT and <2.2.0.M4
Greenwich.SR2	| Spring Boot >=2.1.0.RELEASE and <2.1.9.BUILD-SNAPSHOT
Greenwich.M1	| Spring Boot >=2.1.0.M3 and <2.1.0.RELEASE
Finchley.BUILD-SNAPSHOT	| Spring Boot >=2.0.999.BUILD-SNAPSHOT and <2.1.0.M3
Finchley.SR4	| Spring Boot >=2.0.3.RELEASE and <2.0.999.BUILD-SNAPSHOT
Finchley.RC2	| Spring Boot >=2.0.2.RELEASE and <2.0.3.RELEASE
Finchley.RC1	 | Spring Boot >=2.0.1.RELEASE and <2.0.2.RELEASE
Finchley.M9	| Spring Boot >=2.0.0.RELEASE and <=2.0.0.RELEASE
Finchley.M7	| Spring Boot >=2.0.0.RC2 and <=2.0.0.RC2
Finchley.M6	| Spring Boot >=2.0.0.RC1 and <=2.0.0.RC1
Finchley.M5	| Spring Boot >=2.0.0.M7 and <=2.0.0.M7
Finchley.M4	| Spring Boot >=2.0.0.M6 and <=2.0.0.M6
Finchley.M3	| Spring Boot >=2.0.0.M5 and <=2.0.0.M5
Finchley.M2	| Spring Boot >=2.0.0.M3 and <2.0.0.M5
Edgware.SR5	| 1.5.20.RELEASE
Edgware.SR5	| 1.5.16.RELEASE
Edgware.RELEASE	| 1.5.9.RELEASE
Dalston.RC1	| 1.5.2.RELEASE