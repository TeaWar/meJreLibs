package me.java.library.db.mybatis.config;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import me.java.library.utils.spring.factory.YamlPropertySourceFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

@Slf4j
@Configuration
@AllArgsConstructor
@PropertySource(factory = YamlPropertySourceFactory.class, value = {"classpath:druid-dynamic.yml"})
public class DynamicDruidConfiguration {

    @PostConstruct
    public void init() {
        log.info("### DynamicDruidConfiguration");
    }
}
