package me.java.library.db.mybatis.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import me.java.library.db.mybatis.mapper.SysRoleMapper;
import me.java.library.db.mybatis.model.SysRole;
import me.java.library.db.mybatis.service.SysRoleService;
import org.springframework.stereotype.Service;

/**
 * 角色
 *
 * @author: stars
 * @date 2020年 07月 09日 11:58
 **/
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

}