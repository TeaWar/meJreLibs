package me.java.library.db.mybatis.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import me.java.library.db.mybatis.model.SysRole;

/**
 * 角色信息
 **/
public interface SysRoleMapper extends BaseMapper<SysRole> {
}