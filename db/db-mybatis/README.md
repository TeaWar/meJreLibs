## 动态数据源

[dynamic-datasource-spring-boot-starter](https://github.com/baomidou/dynamic-datasource-spring-boot-starter/blob/master/README.md)

[多数据源及源码分析](https://blog.csdn.net/w57685321/article/details/106823660/)

## Druid连接池

[Druid常见问题](https://github.com/alibaba/druid/wiki/%E5%B8%B8%E8%A7%81%E9%97%AE%E9%A2%98)

[druid-spring-boot-starter应用](https://github.com/alibaba/druid/tree/master/druid-spring-boot-starter)